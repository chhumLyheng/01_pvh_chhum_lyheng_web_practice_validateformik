import React, { Component } from "react";
import { Formik } from "formik";
import * as Yup from "yup";

export default class FormikValidate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      obj: [],
    };
  }

  componentDidUpdate = () => {
    console.log(this.state.obj);
  };
  render() {
    return (
      <div>
        <Formik
          initialValues={{ name: "", phoneNum: "" }}
          validationSchema={Yup.object({
            name: Yup.string()
              .max(15, "Must be 15 characters or less")
              .required("You have to Input Name!"),

            phoneNum: Yup.number()
              .typeError("Please Input the only number")
              .max(12, "Must be 12 characters or less")
              .required("You have to Input PhoneNumber!"),
          })}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values);
            setTimeout(() => {
              const newObj = {
                newName: values.name,
                newphoneNum: values.phoneNum,
              };

              this.setState({
                obj: [...this.state.obj, newObj],
              });

              setSubmitting(false);
            }, 400);
          }}
        >
          {(formik) => (
            <form onSubmit={formik.handleSubmit}>
              {/* Input Name */}
              <label htmlFor="name">Name</label>
              <input id="name" type="text" {...formik.getFieldProps("name")} />
              {formik.touched.name && formik.errors.name ? (
                <div>{formik.errors.name}</div>
              ) : null}
              <br />
              <br />

              {/* Input PhoneNumber*/}
              <label htmlFor="phoneNum">PhoneNumber</label>
              <input
                id="phoneNum"
                type="text"
                {...formik.getFieldProps("phoneNum")}
              />
              {formik.touched.phoneNum && formik.errors.phoneNum ? (
                <div>{formik.errors.phoneNum}</div>
              ) : null}
              <br />
              <br />

              {/* Button Submit */}
              <button type="submit">Submit</button>
              <br />
              <br />
            </form>
          )}
        </Formik>

        {/* Table Display */}
        <center>
          <table border="1">
            <thead>
              <tr>
                <th>Name</th>
                <th>PhoneNumber</th>
              </tr>
            </thead>
            <tbody>
              {this.state.obj.map((m) => (
                <tr>
                  <td>{m.newName}</td>
                  <td>{m.newphoneNum}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </center>
      </div>
    );
  }
}
