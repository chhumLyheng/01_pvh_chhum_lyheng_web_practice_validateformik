import logo from './logo.svg';
import './App.css';
import FormikValidate from './Components/FormikValidate';

function App() {
  return (
    <div className="App">
      <FormikValidate/>
    </div>
  );
}

export default App;
